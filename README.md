Spider
===

Demo 工程
---

首先克隆该仓库，然后在`AndroidStudio`中直接编译完成后点击运行app

接入方式
---

可视化埋点SDK总共需要接入两部份内容，编译插件spider-plugin、spider-sdk
Spider-plugin和spider-sdk接入：
##(1)方法一（离线模式）：
###spider-plugin接入步骤

1）将Spider-Respository.zip解压到工程文件根目录，类似于如下图：

2）在根目录build.gralde文件配置仓库文件和编译依赖类似于：

    buildscript {
	    repositories {
	    	maven { url uri('./Spider-Respository')}
	    }
	    dependencies {
	   		classpath 'com.android.tools.build:gradle:2.2.3'
	    	classpath 'com.qq.reader.statistics:spider-plugin:1.0.0'
	    }
    }

maven { url uri('./Spider-Respository')}：本地仓库地址

classpath 'com.qq.reader.statistics:spider-plugin:1.0.0'：编译插件用于动态修改代码





3）在android主工程build.gradle文件中引入编译插件：

apply plugin: 'com.qq.reader.statistics'

4）在android工程build.gradle中导入自己需要的配置

	SpiderStatistics {
		debug = false //debug模式下打印编译过程中的日志信息
		disableJar = true //是否过滤第三方jar文件，如果使用tinker替换过application的必须是false
		useInclude = false //是否使用incluede方式,如果使用include方式的话,则只扫描include内的类
		lambdaEnabled = false //是否支持lambda表达式
		exclude = []//不包括的方类,使用startWidth判断
		include = []//使用include模式下,所包括的类
		hookApplication = false//是否动态替换Application的父类
	}

###Spider-sdk接入步骤：

1）同spider-plugin第一步

2）在主工程的build.gradle文件内配置本地仓库

	repositories {
		  //...新增
	    maven { url uri('../Spider-Respository')}
	}

3）在dependencies中配置

	dependencies {
		 //...新增
		 compile 'com.qq.reader.statistics:spider-sdk:1.0.0@aar'//统计
	     debugCompile 'com.qq.reader.statistics:spider-selector:1.0.0@aar'//圈选
	}

##(2)方法二（阅文内部maven方式）：（暂时还没有上传maven）
1）该方法和离线模式下唯一的区别是本地仓库修改为内网仓库

2）只需要把maven { url uri('./Spider-Respository')}修改成
	
	maven｛url xxxxxxxx｝

3）其他配置保持不变


快速上手
application中配置初始化sdk

	@Override
	public void onCreate() {
	    super.onCreate();
	    SpiderSDK.start(this, new Configuration()
	            .debug(true)
	            .trackActivity()
	            .trackFragment()
	            .trackView()
	            .percentOfShown(0.75f)//不设置默认0.75f
	            .uploadDelay(200)//默认200，可以不设置
	            .eventReporter(new IEventReporter() {
	        @Override
	        public void reportEvent(Event... events) {
	            for (Event singleEvent : events) {
	                Log.i("SpiderSDK", singleEvent.toString());
	            }
	        }
	    }));
	    //debug包初始化这个功能
	    UISelectWindow.getInstance().init(this, new HeaderProvider() {
	        @Override
	        public Map<String, String> getHeader() {
	            String jsonStr = "{\"loginType\":\"1\",\"server_sex\":\"2\",\"sid\":\"\",\"qrem\":\"0\",\"imei\":\"863817038093483\",\"safekey\":\"8DC1B26A688B34B91705FF42484E2911\",\"c_version\":\"qqreader_7.0.8.0888_android\",\"channel\":\"00000\",\"ywguid\":\"3537879607\",\"cookie\":\"ywkey\\u003dywdjp9CcXDye\",\"supportmh\":\"1\",\"qrtm\":\"1563535536\",\"nosid\":\"1\",\"mac\":\"C4:0B:CB:84:AD:99\",\"gselect\":\"3\",\"ywkey\":\"ywdjp9CcXDye\",\"qimei\":\"c9f82349a24d0ca\",\"os\":\"miui\",\"c_platform\":\"android\",\"supportTS\":\"2\",\"ckey\":\"000000101100\",\"mversion\":\"7.0.8\",\"qrsy\":\"DC7AA74561247C06DE0BEEF2570A5B89\",\"tinkerid\":\"qqreader_7.0.8.0888_android-common-970e43f\",\"qqnum\":\"3537879607\",\"ua\":\"capricorn#capricorn#24\",\"themeid\":\"1000\"}";
	            return new Gson().fromJson(jsonStr, HashMap.class);
	        }
	    },true);
	    UISelectWindow.getInstance().createWindow();
	}
	




绑定方法
---
	详见demo和接入文档

文档
---

- [接入文档](接入文档.md)
- [README](README.md)


作者
---

QQ阅读终端研发团队，
qqreader_mobile_tech@yuewen.com